const express = require("express");
const winston = require("winston");
const expressWinston = require("express-winston");
const { createHttpTerminator } = require("http-terminator");
const { knex } = require("./src/config");
const healthz = require("./src/routes/healthz");
const people = require("./src/routes/people");
const peopleByUuid = require("./src/routes/people_by_uuid");

const app = express();

/**
 * Start the server and list for connections
 */
const server = app.listen(process.env.PORT || 3000);

/**
 * Create an instance to monitor connections when they occur
 */
const httpTerminator = createHttpTerminator({ server });

/**
 * Set the DB middleware so we can use the connection pool in routes
 */
const db = function (req, res, next) {
  req.knex = knex;
  next();
};

/**
 * Set body parsing middleware
 */
app.use(express.json());

/**
 * Setup helpful access logging in Winston
 */
app.use(
  expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
    meta: true,
    msg: "HTTP {{req.method}} {{req.url}}",
    expressFormat: true,
    colorize: true,
  })
);

/**
 * Disable sever identifier
 */
app.disable("x-powered-by");

/**
 * Set the DB middleware
 */
app.use(db);

/**
 * Set the routing middleware
 */
app.use(healthz);
app.use(people);
app.use(peopleByUuid);

/**
 * Default root route
 */
app.get("/", (req, res) => {
  res.sendStatus(200);
});

/**
 * Setup helpful error logging in Winston
 */
app.use(
  expressWinston.errorLogger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
  })
);

/**
 * Catch signals and gracefully shutdown
 */
process.once("SIGINT", function (code) {
  console.log("Stopping the server...");
  httpTerminator.terminate();
});

process.once("SIGTERM", function (code) {
  console.log("Stopping the server...");
  httpTerminator.terminate();
});

module.exports = app;
