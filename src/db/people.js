const { v4: uuidv4 } = require("uuid");
const _ = require("lodash");

/**
 * List all people in the passengers table
 *
 * @param {Object} - Knex object with the needed DB
 * @returns {Promise<Array>} An array object of each person's info
 */
exports.list = function list(knex) {
  return knex
    .select(
      "uuid",
      "survived",
      "passenger_class",
      "name",
      "sex",
      "age",
      "siblings_or_spouses_aboard",
      "parents_or_children_aboard",
      "fare"
    )
    .table("passengers");
};

/**
 * Add a person to the passengers table
 *
 * @param {Object} - Knex object with the needed DB
 * @param {Object} - Person object
 * @returns {Promise<Object>} An object containing the added person's info
 */
exports.add = function add(knex, data) {
  const row = {
    uuid: uuidv4(),
    survived: data.survived,
    passenger_class: data.passengerClass,
    name: data.name,
    sex: data.sex,
    age: data.age,
    siblings_or_spouses_aboard: data.siblingsOrSpousesAboard,
    parents_or_children_aboard: data.parentsOrChildrenAboard,
    fare: data.fare,
  };

  return knex
    .insert(row)
    .table("passengers")
    .then(() => {
      return knex
        .select(
          "uuid",
          "survived",
          "passenger_class",
          "name",
          "sex",
          "age",
          "siblings_or_spouses_aboard",
          "parents_or_children_aboard",
          "fare"
        )
        .where({
          uuid: row.uuid,
        })
        .table("passengers");
    });
};

/**
 * Get a person's information by UUID from the passengers table
 *
 * @param {Object} - Knex object with the needed DB functions
 * @param {String} - UUID of the person to lookup
 * @returns {Promise<Array>} An array object of the person's info
 */
exports.get = function get(knex, uuid) {
  return knex
    .select(
      "uuid",
      "survived",
      "passenger_class",
      "name",
      "sex",
      "age",
      "siblings_or_spouses_aboard",
      "parents_or_children_aboard",
      "fare"
    )
    .where({
      uuid: uuid,
    })
    .table("passengers");
};

/**
 * Delete a person's information by UUID from the passengers table
 *
 * @param {Object} - Knex object with the needed DB functions
 * @param {String} - UUID of the person to delete
 * @returns {Promise} - provides the number of rows removed
 */
exports.del = function del(knex, uuid) {
  return knex
    .where({
      uuid: uuid,
    })
    .table("passengers")
    .del();
};

/**
 * Update a person in the passengers table
 *
 * @param {Object} - Knex object with the needed DB
 * @param {Object} - Person object
 * @param {String} - UUID of the person to update
 * @returns {Promise<Object>} An object containing the added person's info
 */
exports.update = function update(knex, data, uuid) {
  const row = {
    uuid: uuid,
    survived: data.survived,
    passenger_class: data.passengerClass,
    name: data.name,
    sex: data.sex,
    age: data.age,
    siblings_or_spouses_aboard: data.siblingsOrSpousesAboard,
    parents_or_children_aboard: data.parentsOrChildrenAboard,
    fare: data.fare,
  };

  return knex
    .count({ count: "uuid" })
    .where({
      uuid: uuid,
    })
    .table("passengers")
    .then((results) => {
      if (results.count !== 0) {
        return knex
          .update(row)
          .where({
            uuid: uuid,
          })
          .table("passengers");
      }
    });
};
