const express = require("express");
const _ = require("lodash");
const db = require("../db/people");

const router = express.Router();

router
  .route("/people")
  /**
   * Get all people
   */
  .get((req, res, next) => {
    db.list(req.knex)
      .then((results) => {
        res.json(results);
      })
      .catch((error) => {
        next(error);
      });
  })
  /**
   * Add a new person
   */
  .post((req, res, next) => {
    if (_.isUndefined(req.body) || _.isEmpty(req.body)) {
      const error = new Error("Missing request body");
      next(error);
    } else {
      db.add(req.knex, req.body)
        .then((results) => {
          res.json(results[0]);
        })
        .catch((error) => {
          next(error);
        });
    }
  })
  .put((req, res) => {
    res.send("Update the book");
  });

module.exports = router;
