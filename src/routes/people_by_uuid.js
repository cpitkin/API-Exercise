const express = require("express");
const _ = require("lodash");
const db = require("../db/people");

const router = express.Router();

router
  .route("/people/:uuid")
  /**
   * Get a single person by uuid
   */
  .get((req, res, next) => {
    db.get(req.knex, req.params.uuid)
      .then((results) => {
        if (_.isEmpty(results)) {
          res.sendStatus(404);
        } else {
          res.json(results[0]);
        }
      })
      .catch((error) => {
        next(error);
      });
  })
  /**
   * Delete a single person by uuid
   */
  .delete((req, res, next) => {
    db.del(req.knex, req.params.uuid)
      .then((results) => {
        if (results === "0") {
          res.sendStatus(404);
        } else {
          res.sendStatus(200);
        }
      })
      .catch((error) => {
        next(error);
      });
  })
  /**
   * Update a single person by uuid
   */
  .put((req, res, next) => {
    db.update(req.knex, req.body, req.params.uuid)
      .then((results) => {
        if (results === "0") {
          res.sendStatus(404);
        } else {
          res.sendStatus(200);
        }
      })
      .catch((error) => {
        next(error);
      });
  });

module.exports = router;
