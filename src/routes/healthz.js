const express = require("express");
const router = express.Router();

/**
 * A health endpoint that just returns a 200
 */
router.get("/healthz", function (req, res) {
  res.sendStatus(200);
});

module.exports = router;
