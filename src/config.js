const _ = require("lodash");
const knexfile = require("../knexfile");

/**
 * DB connection options
 *
 * We do a post process, pre-user transform of some data.
 * We update the survived from an integer to a boolean
 * We update the object keys to be camel case
 */
const connectionOptions = {
  postProcessResponse: (result, queryContext) => {
    if (Array.isArray(result)) {
      return result.map((row) => {
        if (row.survived === 0) {
          row.survived = false;
        } else {
          row.survived = true;
        }
        return _.mapKeys(row, (value, key) => {
          return _.camelCase(key);
        });
      });
    } else {
      return _.camelCase(result);
    }
  },
  ...knexfile[process.env.NODE_ENV],
};

/**
 * Initialize and export a DB connection
 */
module.exports.knex = require("knex")(connectionOptions);
