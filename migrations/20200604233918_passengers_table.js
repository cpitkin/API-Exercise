exports.up = function (knex) {
  /**
   * Create passengers table if it doesn't exist
   *
   * @returns {Promise} The query status
   */
  return knex.schema
    .hasTable("passengers")
    .then((exists) => {
      if (!exists) {
        return knex.schema.createTable("passengers", (table) => {
          table.increments();
          table.boolean("survived");
          table.integer("passenger_class");
          table.string("name");
          table.enu("sex", ["male", "female", "other"]);
          table.integer("age");
          table.integer("siblings_or_spouses_aboard");
          table.integer("parents_or_children_aboard");
          table.decimal("fare", 10, 4);
          table.timestamp("created_at").defaultTo(knex.fn.now());
        });
      }
    })
    .catch((error) => {
      console.error(error);
    });
};

exports.down = function (knex) {
  // Not the best to put destructive actions here
};
