// Database configurations for Knex

module.exports = {
  development: {
    client: "mysql",
    connection: {
      database: "titanic",
      user: "user",
      password: "password",
      host: "db",
    },
    migrations: {
      tableName: "knex_migrations",
    },
    seeds: {
      directory: "./seeds/default",
    },
  },

  test: {
    client: "mysql",
    connection: {
      database: "test_titanic",
      user: "user",
      password: "password",
      host: "test_db",
      post: 3307,
    },
    migrations: {
      tableName: "knex_migrations",
    },
    seeds: {
      directory: "./seeds/default",
    },
  },

  staging: {
    client: "mysql",
    connection: {
      database: process.env.MYSQL_DATABASE,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      host: process.env.MYSQL_HOST,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
    seeds: {
      directory: "./seeds/default",
    },
  },

  production: {
    client: "mysql",
    connection: {
      database: process.env.MYSQL_DATABASE,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      host: process.env.MYSQL_HOST,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
    seeds: {
      directory: "./seeds/default",
    },
  },
};
