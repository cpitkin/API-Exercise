const request = require("supertest");
const app = require("../index");
const { knex } = require("../src/config");

describe("Post Person", () => {
  it("should create a new person", async () => {
    const reqObj = {
      survived: true,
      passengerClass: 1,
      name: "Mr. Charlie Pitkin",
      sex: "male",
      age: 22,
      siblingsOrSpousesAboard: 1,
      parentsOrChildrenAboard: 0,
      fare: 7.25,
    };

    const res = await request(app).post("/people").send(reqObj);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toMatchObject(reqObj);
  });
});
