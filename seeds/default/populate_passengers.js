const fs = require("fs");
const path = require("path");
const papa = require("papaparse");
const { v4: uuidv4 } = require("uuid");

const csv_file = fs.readFileSync(
  path.resolve(process.env.PWD, "requirements/titanic.csv"),
  "utf8"
);

const config = {
  header: true,
  transformHeader: (header) => {
    switch (header) {
      case "Survived":
        return "survived";
        break;
      case "Pclass":
        return "passenger_class";
        break;
      case "Name":
        return "name";
        break;
      case "Sex":
        return "sex";
        break;
      case "Age":
        return "age";
        break;
      case "Siblings/Spouses Aboard":
        return "siblings_or_spouses_aboard";
        break;
      case "Parents/Children Aboard":
        return "parents_or_children_aboard";
        break;
      case "Fare":
        return "fare";
        break;
      default:
        return header;
    }
  },
};

exports.seed = function (knex) {
  const titanic_csv = papa.parse(csv_file, config);
  /**
   * Truncate the table
   *
   * @returns {Promise} The query status
   */
  return knex("passengers")
    .del()
    .then(() => {
      /**
       * Seed passengers table if it is empty
       *
       * @returns {Promise} The query status
       */
      return knex("passengers")
        .count("id", { as: "passengers" })
        .then((row_count) => {
          if (row_count[0].passengers === 0) {
            const passengers = titanic_csv.data.map((passenger) => {
              if (passenger.survived === "0") {
                passenger.survived = false;
              } else {
                passenger.survived = true;
              }
              passenger.uuid = uuidv4();
              return passenger;
            });

            return knex
              .batchInsert("passengers", passengers, passengers.length)
              .catch((error) => {
                throw new Error(error);
              });
          }
        });
    });
};
