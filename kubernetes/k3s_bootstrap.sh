#!/usr/bin/env bash

set -e

echo "The script will ask for root access so it can install k3s and configure a systemd job."

echo " -- Installing k3s -- "
curl -sfL https://get.k3s.io | sh -s - server --disable traefik --write-kubeconfig ~/.kube/k3s

sudo chown $USER:$USER ~/.kube/k3s

echo " -- Deploying MySQL -- "
KUBECONFIG=~/.kube/k3s kubectl apply -f mysql/

echo " -- Deploying API -- "
KUBECONFIG=~/.kube/k3s kubectl apply -f api/

echo "-- Deploying Traefik -- "
KUBECONFIG=~/.kube/k3s kubectl apply -f traefik/

KUBECONFIG=~/.kube/k3s kubectl get all

echo ""
echo "You can export the Kube config to connect to the cluster."
echo "$ KUBECONFIG=~/.kube/k3s"

echo "To uninstall k3s run"
echo "$ sudo /usr/local/bin/k3s-uninstall.sh"
