# CS-API Documentation

> A basic CRUD REST API to interact with a DB of people from the Titanic

### Tech stack

- MySQL 5.7
- Nodejs 12
- Express
- Docker-Compose - local development
- Prettier - Code formatting and style
- k3s - Local Kubernetes cluster
- DOK - Digital Ocean Kubernetes cloud managed cluster

## Local Development

Local development can be done in two ways as each person likes to do things a little different. For those who like to use VS Code the project can use the [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) extension to allow booting Docker Compose inside the editor. You will find setup instructions from Microsoft [here](https://code.visualstudio.com/docs/remote/containers). For those who like to take the classic, do everything in the terminal, approach the code runs just fine with `docker-compose up`.

Run in VS Code

1. Open the Command Palette in VS Code
2. Select Open Folder in Container
3. Select the appropriate folder from the file explorer
4. Because VS Code overrides the command from compose we will have to install and run migrations manually
   1. `yarn install`
   2. `yarn db`
   3. Run `yarn dev` to start the server is dev mode with automatic restarts on change
5. Enjoy a g by great dev experience

Run with Docker Compose in a terminal

1. Open your Terminal of choice
2. Run `docker-compose up` in the application directory to build and start the application
3. In a separate terminal run `docker-compose exec api /bin/bash` from the application directory so you can run extra yarn commands when needed
4. In the second terminal run `yarn db` to run migrations and seed the DB

## MySQL Database

Migrations

Migrations are used to control tables in the database

Seeding

To bulk insert or not to bulk insert?

I was decided to go with a bulk insert since the CSV file was under 1000 rows. With so few rows it doesn't hurt to have a single transaction. It isn't a huge issue to roll back on so few rows. If we were dealing with tens or hundreds of thousands of rows then you might want single row insert.d

User and host credential management

The knexfile.js houses the DB connection configuration. When in development the application uses the local DB containers linked from Docker Compose. When in staging or production the username, password, and host are all pulled in using environment variables for security. This provides the additional benefit of allowing the runtime environment to control where and how the application connects to a data source. You should never bake credentials or hardcoded configurations into the Docker image. There is very nearly always a better way.

## Express API

The core API is an Express application

### Data transform on post returned DB data

The _survided_ column value needed to be transformed into a boolean from 0 or 1. This shouldn't be needed but knex decided to not do it automatically.

All column keys are transformed into a JSON friendly camel case format from snake case.

### Routing

The route handlers are fairly modular based on the size and scope of the project. Each set of routing files is required in the main application file (_index.js_). Each set of individual routes in held in the `src/routes` directory. This helps to provide good naming to similar routes based on the path they occupy in the API.

### DB connection

The DB connector uses connection pooling so it should only be initialized once then passed into the functions and methods that require it. This is easy with Express as an attached middleware to the request object. The middleware is only loaded once on boot then passed around to any route that needs it. In this way we only initialize the DB connection on startup then pass it to the routes who can choose to use it if needed.

### Extra Routes

GET - /

Returns a 200 OK.

GET - /healthz

Returns a 200 OK. This is the primary health endpoint used by Kubernetes for liveness and readiness checks.

## Signal Handling

The application doesn't handle signals

I decided to implement at least a high level of each of the 6 steps instead of deep diving into any one step. This helps me to demonstrate my ability to work in all parts of the stack and overall lifecycle of an application. At the end of the day implementation details don't matter as much so long as you have a smooth(ish) pipeline from dev to production.

## Testing

Run `yarn test`

I didn't get to add as many tests as I would have liked but the groundwork is in place. More tests can easily be added for all the routes.

## Docker Images

The Docker images are stored in the Gitlab Container Registry. This way the images and code are kept close together for easy reference.

[Registry](http://registry.gitlab.com/cpitkin/api-exercise)

Tags

- node-12_v1
- node-12_v1.1
- node-12_v1.2

## Kubernetes

The deployment files were tested in k3s locally and in Digital Ocean Kubernetes.

Had it been a little easier to share access and a bit more time I would have traded the raw _yaml_ files for a IaC tool like [Pulumi](https://www.pulumi.com/). This way I could have stood up the DOK control plane and nodes then use the same tool to push the application into Kubernetes. It would also have the added benefit of encrypted secrets at rest and traceability of all changes to the infrastructure of application deployments. The deployment files use a _Rolling Update_ strategy so the application is never offline. The the case of only one replica that wouldn't be true but you would typically run more than one.

### Secrets

The secrets for DB access are stored as kubernetes secrets. Right now those secrets are stored in plain text in the repo for purposed of this small demo app. What should really happen is either encrypt the secrets in the repo before check-in or store and retrieve them from a service like Hashicorp Vault.

### k3s

The _boostrap.sh_ script in the kubernetes directory will install, start, and deploy the application and its dependencies to a local single node cluster.

Add the code below to your `/etc/hosts` file to hit the local cluster

```bash
127.0.0.1 spaceage.industries www.spaceage.industries
```

The Traefik server is configured to always upgrade to HTTPS. This isn't an issue with the service running in Digital Ocean but locally the certificate won't work. Traefik generates a default certificate for cases like these so you can safely ignore the warnings and see the page.

### Digital Ocean Kubernetes

The Digital Ocean cluster is 3 nodes with a managed control plane. This allows you to see a fully running setup in a managed cluster that can be hit from the Internet. The certificate was issued by Let's Encrypt and is managed by Traefik. Traefik isn't configured to handle www to bare domain redirects or anything as that is fairly trivial to add across any domain behind the ingress controller.

[https://spaceage.industries/](https://spaceage.industries/)

### Next Steps

#### Application Monitoring

Add a Prometheaus endpoint to the application to allow for metrics to be scrapped.

Deploy Prometheaus and Grafana to the cluster

#### Aggregated logging

Deploy something like Loki and Promtail to the cluster to gather and aggregate logs from all contiainers. Logs can be viewed in the Grafana dashboard.

You could also use a service like Datadog or Loggly.

#### Add Pulumi or Terraform

Use a IaC tool to build and manage the infrastructure

You could also use Pulumi to manage the k8s deployments

#### Yaml templating tool

Kustimize, Jsonnet, Ksonnet, Helm - Just pick your flavor

#### CI/CD

1. Pull the code from the repo
2. Install dependencies
3. Run DB migrations, etc on a test DB
4. Run tests (unit, integration, etc)
5. Build production grade container image
6. Tag and push the image to a container repo
7. Trigger a deployment using the new image tag
8. Monitor for changes
